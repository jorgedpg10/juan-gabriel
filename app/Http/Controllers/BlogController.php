<?php

namespace App\Http\Controllers;

use App\Models\JgPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use TCG\Voyager\Models\Post;

class BlogController extends Controller
{
    public function index() {
        $posts = JgPost::all();
        return View::make('blog.index', ['posts' => $posts]);
    }

    public function show($id) {
        $post = JGPost::findOrfail($id);
        $posts = JgPost::all();
        return View::make('blog.show', ['post' => $post,
                                            'posts' => $posts
            ]);
    }
}
