<?php

namespace App\Http\Controllers;

use App\Models\JgPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;


class IndexController extends Controller
{
    public function index() {
        $posts = JgPost::all();
        return View::make('index', ['posts' => $posts]);
    }
}
