<!-- Navigation Section -->
<div class="navbar custom-navbar wow fadeInDown" data-wow-duration="2s" role="navigation" id="header">
    <div class="container">

        <!-- NAVBAR HEADER -->
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span> <span class="icon icon-bar"></span> <span class="icon icon-bar"></span>
            </button>
            <!-- lOGO TEXT HERE -->
            <div class="conten-logo">
            <a href="{{ route('index') }}" class="navbar-brand">
                <img src="{{ asset('imgs/logo/logo.jpg') }}" class="whtlogo logo1" alt="logo" />
                <img src="{{ asset('imgs/logo/logo.jpg') }}" class="logocolor logo1" alt="logo"></a>
            </div>

        <!-- NAVIGATION LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="{{ route('index') }}">Inicio</a></li>
                <li><a href="{{ route('nosotros') }}">Nosotros</a></li>
                <li class="dropdown"><a href="{{ route('blog.index') }}">Blog</a></li>
                <li><a href="{{ route('contacto') }}">Contacto</a></li>
                <li><span class="calltxt"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;(07)2722218</span></li>
                <li><span class="calltxt"><i class="fab fa-whatsapp"></i>&nbsp;&nbsp;0984527194</span></li>
            </ul>
        </div>
    </div>
</div>
