<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Jus Gentium - @yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="LawFirm One Page HTML Template">
    <meta name="keywords" content="one page, html, template, responsive, business">
    <meta name="author" content="sharjeel anjum">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap css -->
    <link rel="stylesheet" href=" {{ asset('css/bootstrap.min.css') }} ">

    <!-- Fontawesome css -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

    <!-- Magnific-popup css -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}} ">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }} ">

    <!-- Custom css -->
    <link rel="stylesheet" href="{{ asset('css/jorge.css') }} ">
</head>
<body class="subpage" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

@include('secondary-partials.navbar')


@yield('content')

@include('partials.footer')

<!-- Bootstrap -->
<script src="{{ asset('js/jquery-2.1.4.min.js') }} "></script>
<script src="{{ asset('js/bootstrap.min.js') }} "></script>
</body>
</html>
