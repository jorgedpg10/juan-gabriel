@extends('layouts.secondary')

@section('title', 'Nosotros')

@section('content')

    <!-- Page Title start -->
    <div class="pageTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1 class="page-heading">Nosotros</h1>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="breadCrumb">
                        <a href="{{ route('index') }}">Inicio</a> / <a href="{{ route('nosotros') }}">Nosotros</a> / <span>Ab. Juan Gabriel Peralta</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title End -->

    <div class="parallax-section">
        <div class="container">
            <div class="attorneytop">
                <div class="row">
                    <div class="col-md-4 col-sm-4"><img src="{{ asset('imgs/equipo/juan.jpg') }} " class="lawimg" alt="" /></div>
                    <div class="col-md-8 col-sm-8">
                        <h2>Juan Gabriel Peralta</h2>
                        <h3>Abogado</h3>
                        <p>Abogado especialista en derecho constitucional, penal y procesal</p>
                        <ul class="address redes">
                            <li><i class="fab fa-whatsapp"></i>0984527184</li>
                            <li><i class="fa fa-envelope"></i><a href="#">jgperalta12@gmail.com</a></li>
                        </ul>
                        <ul class="list-inline social redes">
                            <li> <a href="javascript:void(0);"><i class="fab fa-facebook" aria-hidden="true"></i></a> </li> {{-- juan gabriel peralta--}}
                            <li> <a href="javascript:void(0);"><i class="fab fa-twitter" aria-hidden="true"></i></a> </li> {{-- @jpagrielperalta --}}
                            <li> <a href="javascript:void(0);"><i class="fab fa-linkedin" aria-hidden="true"></i></a> </li> {{-- https://www.linkedin.com/in/juan-gabriel-peralta-venegas-52611264/--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="attorneydetail">
                <h1>Visión del derecho</h1>
                El sistema judicial es la herramienta mas cercano al ciudadano para exigir el respeto a sus derechos, y el debido proceso el camino para lograrlo
                <div class="row">
                    <div class="col-md-6">
                        <h2>Formación</h2>
                        <ul>
                            <li>Abogado de los tribunales y juzgados por la Universidad Internacional del Ecuador.</li>
                            <li>Especialista en derecho procesal penal por la Universidad Técnica Particular de Loja.</li>
                            <li>Especialista en derecho constitucional por la Univerdad Andina Simón Bolivar.</li>
                            <li>Cursando especialidad en derecho procesal En Universidad Andina Simón Bolivar.</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h2>Experiencia</h2>
                        <ul>
                            <li>13 años en el libre ejercicio de la profesión.</li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="attorneyContact">
                <h3>Contacte al Ab. Juan Gabriel</h3>
                <div class="contact-form contenedor-form">
                    <form id="contact-form" class="row">
                        {{ csrf_field() }}
                        <div class="col-md-4 col-sm-6">
                            <input type="text" class="form-control" name="name" placeholder="Nombre">
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <input type="tel" class="form-control" name="phone" placeholder="Teléfono">
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <input type="text" class="form-control" name="phone" placeholder="Direccicón">
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <textarea class="form-control" rows="5" name="message" placeholder="Mensaje"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button id="submit" type="submit" class="form-control" name="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
