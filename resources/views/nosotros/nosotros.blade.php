@extends('layouts.secondary')

@section('title', 'Nosotros')

@section('content')

    <!-- Page Title start -->
    <div class="pageTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1 class="page-heading">Nosotros</h1>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="breadCrumb"><a href="{{ route('index') }}">Inicio</a> / <span>Nosotros</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title End -->

    <div class="innerpgWraper">
        <div class="container">
            <div class="about-desc">
                <div class="row">
                    <div class="col-md-7">
                        <h3>Jus Gentium - Law Firm</h3>
                        <p style="font-size: 19px; text-align: justify;">
                            Jus Gentium es un vocablo en latín que significa 'derecho de gentes' identificado en la actualidad como 'acceso a la justicia';
                            el estudio de abogados nace en la ciudad de Loja en el año 2009 con un enfoque distinto y el idealismo de unos jóvenes por cambiar
                            la imagen de esta profesión, y acercar al sistema de justicia a las personas que necesitan la tutela de sus derechos a través del órgano
                            jurisdiccional.
                            <br>
                            <br>
                            Durante todos estos años, la ardua tarea nos deja la satisfacción de haber defendido a los ciudadanos con tesón y esfuerzo, usando a la ley
                            como única herramienta de defensa.
                            <br>
                            <br>
                            El compromiso se mantiene firme y las puertas abiertas para atender sus requerimientos.
                        </p>
                    </div>
                    <div class="col-md-5">
                        <div class="postimg"><img src="{{ asset('imgs/equipo/equipo.jpg') }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Team Section -->
    <div id="team" class="parallax-section">
        <div class="container">

            <!-- Dection Title -->
            <div class="section-title">
                <h3>Nuestro<span>Equipo</span></h3>
                <p>Un grupo de excelentes profesionales</p>
            </div>
            <div class="row">
                <!-- team 1 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="{{ asset('imgs/equipo/pedro.jpg') }}" class="animate" alt=""></div>
                        <h4>Pedro Peralta</h4>
                        <h5>Asistente de Despacho</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><a href="{{ route('juan') }}"><img src="{{ asset('imgs/equipo/juan.jpg') }}" class="animate" alt=""></a></div>
                        <h4><a href="{{ route('juan') }}">Juan Gabriel Peralta</a></h4>
                        <h5>Director General</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="{{ asset('imgs/equipo/juan-alejandro.jpg') }}" class="animate" alt=""></div>
                        <h4>Juan Alejandro Monroy</h4>
                        <h5>Asistente de Despacho</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 4 -->

            </div>
        </div>
    </div>

@endsection

