@extends('layouts.secondary')

@section('title', 'Blog')

@section('content')
    <!-- Page Title start -->
    <div class="pageTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1 class="page-heading">Blog</h1>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="breadCrumb"><a href="{{ route('index') }}">Inicio</a> / <span>Blog</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title End -->

    <div class="listpgWraper">
        <div class="container">
            <!-- Blog start -->
            <div class="row">
                <div class="col-md-4">
                    <!-- Side Bar -->
                    <div class="sidebar">
                        <!-- Search -->
                        <div class="widget">
                            <h5 class="widget-title">Buscar</h5>
                            <div class="search">
                                <form>
                                    <input type="text" class="form-control" placeholder="Buscar">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <!-- Categories -->
                        {{--<div class="widget">
                            <h5 class="widget-title">Categories</h5>
                            <ul class="categories">
                                <li><a href="#.">Popular posts</a></li>
                                <li><a href="#.">Productivity</a></li>
                                <li><a href="#.">Resumes</a></li>
                                <li><a href="#.">Women</a></li>
                                <li><a href="#.">Top Companies</a></li>
                                <li><a href="#.">Popular posts</a></li>
                                <li><a href="#.">Productivity</a></li>
                                <li><a href="#.">Resumes</a></li>
                            </ul>
                        </div>--}}
                        <!-- Recent Posts -->
                        {{--<div class="widget">
                            <h5 class="widget-title">Recent Posts</h5>
                            <ul class="papu-post">
                                <li>
                                    <div class="media-left"> <a href="#."><img src="images/blog/1.jpg" alt="Blog Title"></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#">Integer vel magna urna. Vestibulum id nisi</a> <span>Dec 18, 2016</span> </div>
                                </li>
                                <li>
                                    <div class="media-left"> <a href="#."><img src="images/blog/2.jpg" alt="Blog Title"></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#">Integer vel magna urna. Vestibulum id nisi</a> <span>Dec 18, 2016</span> </div>
                                </li>
                                <li>
                                    <div class="media-left"> <a href="#."><img src="images/blog/3.jpg" alt="Blog Title"></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#">Integer vel magna urna. Vestibulum id nisi</a> <span>Dec 18, 2016</span> </div>
                                </li>
                                <li>
                                    <div class="media-left"> <a href="#."><img src="images/blog/4.jpg" alt="Blog Title"></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#">Integer vel magna urna. Vestibulum id nisi</a> <span>Dec 18, 2016</span> </div>
                                </li>
                            </ul>
                        </div>--}}
                        <!-- Archives Posts -->
                        {{--<div class="widget">
                            <h5 class="widget-title">Archives</h5>
                            <ul class="archive">
                                <li><a href="#.">June 2015<span>10</span></a></li>
                                <li><a href="#.">May 2015<span>21</span></a></li>
                                <li><a href="#.">April2015 <span>58</span></a></li>
                                <li><a href="#.">March 2015 <span>25</span></a></li>
                            </ul>
                        </div>--}}
                        <!-- Photos -->
                        <div class="widget">
                            <h5 class="widget-title">Photos Streem</h5>
                            <ul class="photo-steam">
                                @foreach($posts as $post)
                                <li><a href="{{ route('blog.show', $post->id) }}"><img src="{{ asset('/storage/'.$post->image) }}" alt=""></a></li>

                                @endforeach
                            </ul>
                        </div>
                        <!-- Tags -->
                        <div class="widget">
                            <h5 class="widget-title">Etiquetas</h5>
                            <ul class="tags">
                                <li><a href="#.">Derecho </a></li>
                                <li><a href="#.">Constitucional </a></li>
                                <li><a href="#.">Penal </a></li>
                                <li><a href="#.">Procesal </a></li>
                                <li><a href="#.">Fiscalía </a></li>
                                <li><a href="#.">Política </a></li>

                            </ul>
                        </div>
                    </div>
                </div>


                <div class="col-md-8">
                    <!-- Blog List start -->
                    <div class="blogWraper">
                        <ul class="blogList">
                            @foreach($posts as $post)
                            <li>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="postimg"><img src="{{ asset('/storage/'.$post->image) }}" alt="Blog Title">
                                            <div class="date">{{ date('d-M', strtotime($post->created_at)) }}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="post-header">
                                            <h4><a href="{{ route('blog.show', $post->id) }}">{{ $post->title }}</a></h4>
                                            <div class="postmeta">Por : <span>{{ $post->autor }} </span> </div>
                                        </div>
                                        <p>{{ $post->excerpt }}</p>
                                        <a href="#" class="readmore">Más</a> </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <!-- Pagination -->
                    <div class="pagiWrap">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="showreslt">Showing 1-1</div>
                            </div>
                            <div class="col-md-8 col-sm-6 text-right">
                                <ul class="pagination">
                                    <li class="active"><a href="#.">1</a></li>
                                    {{--<li><a href="#.">2</a></li>
                                    <li><a href="#.">3</a></li>
                                    <li><a href="#.">4</a></li>
                                    <li><a href="#.">5</a></li>
                                    <li><a href="#.">6</a></li>
                                    <li><a href="#.">7</a></li>
                                    <li><a href="#.">8</a></li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
