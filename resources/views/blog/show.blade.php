@extends('layouts.secondary')

@section('title', 'Blog')

@section('content')
    <!-- Page Title start -->
    <div class="pageTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1 class="page-heading">Blog</h1>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="breadCrumb"><a href="#.">Home</a> / <span>Blog</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title End -->

    <div class="listpgWraper">
        <div class="container">
            <!-- Blog start -->
            <div class="row">
                <div class="col-md-8">
                    <!-- Blog List start -->
                    <div class="blogWraper">
                        <div class="blogList blogdetailbox">
                            <div class="postimg"><img src="{{  asset('/storage/'.$post->image) }}" alt="Blog Title">
                                <div class="date"> 17 SEP</div>
                            </div>
                            <div class="post-header margin-top30">
                                <h4><a href="#.">{{ $post->title }}</a></h4>
                                <div class="postmeta">Por: <span>{{ $post->autor }} </span></div>
                            </div>
                            {!! $post->body !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- Side Bar -->
                    <div class="sidebar">
                        <!-- Search -->
                        <div class="widget">
                            <h5 class="widget-title">Search</h5>
                            <div class="search">
                                <form>
                                    <input type="text" class="form-control" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>



                        <!-- Photos -->
                        <div class="widget">
                            <h5 class="widget-title">Photos Streem</h5>
                            <ul class="photo-steam">
                                @foreach($posts as $post)
                                    <li><a href="{{ route('blog.show', $post->id) }}"><img src="{{ asset('/storage/'.$post->image) }}" alt=""></a></li>

                                @endforeach
                            </ul>
                        </div>
                        <!-- Tags -->
                        <div class="widget">
                            <h5 class="widget-title">Etiquetas</h5>
                            <ul class="tags">
                                <li><a href="#.">Derecho </a></li>
                                <li><a href="#.">Constitucional </a></li>
                                <li><a href="#.">Penal </a></li>
                                <li><a href="#.">Procesal </a></li>
                                <li><a href="#.">Fiscalía </a></li>
                                <li><a href="#.">Política </a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
