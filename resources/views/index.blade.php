@extends('layouts.app')

@section('title', 'Inicio')

@section('content')
    @include('index-partials.navbar')
    <!-- Revolution slider start -->
    <div class="tp-banner-container" id="slider">
        <div class="tp-banner">
            <ul>

                <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"><img
                        alt="" src="{{ asset('imgs/slider/biblioteca-70.jpg') }}"
                        data-lazyload="{{ asset('imgs/slider/biblioteca-70.jpg') }}">
                    <div class="caption lfb large-title tp-resizeme slidertext2" data-x="center" data-y="280"
                         data-speed="600" data-start="1000">Soluciones Jurídicas en Loja
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext1" data-x="center" data-y="310"
                         data-speed="600" data-start="1600">JG Law Firm
                    </div>
                    <div class="caption lfb large-title tp-resizeme sliderbtn" data-x="center" data-y="400"
                         data-speed="600" data-start="2200"><a href="{{ route('nosotros') }}" class="boton-hero" >Nosotros</a></div>
                </li>

                <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"><img
                        alt="" src="{{ asset('imgs/slider/pilares-verde-80.jpg') }}"
                        data-lazyload="{{ asset('imgs/slider/pilares-verde-80.jpg') }}">
                    <div class="caption lfb large-title tp-resizeme slidertext1" data-x="right" data-y="200"
                         data-speed="600" data-start="1000">Bienvenidos JG Law Firm
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext1" data-x="right" data-y="240"
                         data-speed="600" data-start="1600">Una Firma Responsable
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext2" data-x="right" data-y="300"
                         data-speed="600" data-start="2200"> "La ley es razón libre de pasión"
                    </div>
                    <div class="caption lfb large-title tp-resizeme slidertext2" data-x="right" data-y="340"
                         data-speed="600" data-start="2200">Sócrates
                    </div>


                    <div class="caption lfb large-title tp-resizeme sliderbtn" data-x="right" data-y="370"
                         data-speed="600" data-start="2800"><a href="{{ route('nosotros') }}" class="boton-hero">Nosotros</a></div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Revolution slider end -->

    <!-- About section -->
    <div class="howitwrap" id="about">
        <div class="fullimg" style="background-image:url(imgs/index/abogado.jpg)"></div>
        <div class="stcontent">

            <!-- title start -->
            <div class="section-title">
                <h3>Bienvenido a <span>JG Law Firm</span></h3>
                <p style="text-align: center;">"Si quieres la paz, lucha por la justicia" (Pablo VI)</p>
            </div>
            <!-- title end -->

            <ul class="howlist">
                <!--step 1-->
                <li>
                    <div class="howbox">
                        <div class="iconcircle"><i class="fa fa-university" aria-hidden="true"></i></div>
                        <h4>Derecho Constitucional</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid ut
                            labore</p>--}}
                    </div>
                </li>
                <!--step 1 end-->

                <!--step 2-->
                <li>
                    <div class="howbox">
                        <div class="iconcircle"><i class="fas fa-gavel" aria-hidden="true"></i></div>
                        <h4>Derecho Penal</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid ut
                            labore</p>--}}
                    </div>
                </li>
                <!--step 2 end-->

                <!--step 3-->
                <li>
                    <div class="howbox">
                        <div class="iconcircle"><i class="fa fa-male" aria-hidden="true"></i></div>
                        <h4>Derecho Procesal</h4>
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidid ut
                            labore</p>--}}
                    </div>
                </li>
                <!--step 3 end-->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <!-- Counter Section -->
    <div id="counter">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="400" data-speed="1000"></span> <span
                            class="counter-text">Clientes satisfechos</span></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="1200" data-speed="2000"></span> <span
                            class="counter-text">Audiencias</span></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fas fa-user-tie" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="13" data-speed="3000"></span> <span
                            class="counter-text">Años de experiencia</span></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 counter-item">
                    <div class="counterbox">
                        <div class="counter-icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>
                        <span class="counter-number" data-from="1" data-to="4" data-speed="4000"></span> <span
                            class="counter-text">Posgrados</span></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Practice Areas section -->
    <div id="practicearea" class="parallax-section">
        <div class="container">
            <!-- Section Title -->
            <div class="section-title">
                <h3>Áreas de <span>Especialidad</span></h3>
                <p>Contamos con títulos de cuarto nivel en las siguientes áreas del derecho.</p>
            </div>
            <div class="row">
                <!-- Service 1 -->
                <div class="col-md-4 col-sm-6">
                    <div class="service-thumb">
                        <div class="thumb-img"><img src="{{ asset('imgs/tipos-derecho/martillo.jpg') }}" alt=""></div>
                        <h4>Derecho constitucional</h4>
                        <p>Permite tutelar el derecho de las personas, a través de las garantías jurisdiccionales</p>
                    </div>
                </div>

                <!-- Service 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="service-thumb">
                        <div class="thumb-img"><img src="{{ asset('imgs/tipos-derecho/esposas.jpg') }}" alt=""></div>
                        <h4>Derecho Penal</h4>
                        <p>Regula la potestad punitiva, es decir que regula la actividad criminal dentro de un
                            Estado.</p>
                        <p></p>
                    </div>
                </div>

                <!-- Service 3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="service-thumb">
                        <div class="thumb-img"><img src="{{ asset('imgs/tipos-derecho/justicia.jpg') }}" alt=""></div>
                        <h4>Derecho Procesal</h4>
                        <p>Defensa efectiva en las áreas administrativa, civil y laboral.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- barrita negra -->
    {{--<div class="servicesbox bg1">
        <div class="container">
            <div class="section-title">
                <h3>Personal Injury Lawyers</h3>
            </div>
            <div class="ctoggle">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tincidunt mauris est, in faucibus
                    dui viverra et. Aliquam finibus vestibulum elit, at pharetra nisl congue vel. Nunc pretium posuere
                    justo pretium fringilla. Sed volutpat risus non rhoncus convallis. Sed fermentum est at hendrerit
                    pellentesque. Mauris nec leo euismod, sagittis mauris in, posuere est...</p>
                <a href="#" class="readmore">Read More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>--}}

    <!-- Team Section -->
    <div id="team" class="parallax-section">
        <div class="container">

            <!-- Dection Title -->
            <div class="section-title">
                <h3>Nuestro<span>Equipo</span></h3>
                <p>Un grupo de excelentes profesionales</p>
            </div>
            <div class="row">
                <!-- team 1 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="{{ asset('imgs/equipo/pedro.jpg') }}" class="animate" alt=""></div>
                        <h4>Pedro Peralta</h4>
                        <h5>Asistente de Despacho</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 2 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="{{ asset('imgs/equipo/juan.jpg') }}" class="animate" alt=""></div>
                        <h4>Juan Gabriel Peralta</h4>
                        <h5>Director General</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="team-thumb">
                        <div class="thumb-image"><img src="{{ asset('imgs/equipo/juan-alejandro.jpg') }}" class="animate" alt=""></div>
                        <h4>Juan Alejandro Monroy</h4>
                        <h5>Asistente de Despacho</h5>
                        <ul class="list-inline social">
                            <li><a href="javascript:void(0);" class="bg-twitter"><i class="fab fa-twitter"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-facebook"><i class="fab fa-facebook"
                                                                                     aria-hidden="true"></i></a></li>
                            <li><a href="javascript:void(0);" class="bg-linkedin"><i class="fab fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- team 4 -->

            </div>
        </div>
    </div>

    <!-- Tagline Section -->
    <div class="taglinewrap">
        <div class="container">
            <h2>Llámenos, para agendar su cita</h2>
            <h3>(07) 2722218</h3>
            <p>"El buen ciudadano es aquel que no puede tolerar en su patria un poder que pretenda hacerse superior
                a las leyes." - Cicerón</p>
            <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> Contacto</a></div>
    </div>

    <!-- FAQ Section -->
    <div id="faqs" class="parallax-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-faq">
                    <div class="faqs">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class=""
                                                               href="#collapse1">
                                            ¿Pueden obligarme a vacunarme por covid-19?
                                        </a></h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">El árt. 66 de la constitución reconoce entre los derechos de
                                        libertad
                                        el de libre desarrollo de la personalidad, por lo que nadie puede ser oblidago a
                                        vacunarse.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse2">
                                            ¿Puedo ir preso por manejar en estado de ebriedad?
                                        </a></h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body"> Conducir bajo estado de ebredad conlleva una sanción
                                        privativa
                                        de libertad proporcional al grado de alcohol que se encuentre en la sangre.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse3">
                                            ¿Son legítimas las fotomultas de tránsito de la alcaldía de Loja?
                                        </a></h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">Existen vairias resoluciones judiciales que han declarado
                                        la invalidez de fotomultas de tránsito en la ciudad de Loja, por cuanto la
                                        alcaldía violentó los derechos de las personas.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse4">¿Que derechos tengo
                                            como trabajador?</a></h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">Los trabajadores gozan de una amplia protección relacionada
                                        principalmente a su
                                        estabilidad y seguridad social, y a ser indemnizados en caso de que su empleador
                                        los despida.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion"
                                                               class="collapsed" href="#collapse5">¿Qué impuestos se
                                            deben pagar al adquirir una vivienda?</a></h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">La transferencia de dominio de un inmueble genera un
                                        impuesto a la utilidad
                                        que debe pagar el vendedor e impuestos de alcabala, trámite notarial e
                                        inscripción en el registro de
                                        la propiedad que debe asumir el comprador.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-md-6">

                    <!-- Video start -->
                    <div class="videowraper">
                        <div class="videobg">
                            <div class="video-image" style="background-image:url(images/videobg.jpg)"></div>
                            <div class="playbtn"><a href="#." class="popup" data-toggle="modal"
                                                    data-target="#watchvideo"><span></span></a></div>
                        </div>
                    </div>
                    <!-- Video end -->

                    <!-- Video Modal -->
                    <div class="modal fade" id="watchvideo" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="video-container">
                                        <iframe width="100%" height="310"
                                                src="https://www.youtube.com/embed/7e90gBu4pas" frameborder="0"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>

    <!-- Testimonials Section -->
    {{--<div id="testimonials">
        <div class="container">

            <!-- Section Title -->
            <div class="section-title">
                <h3>Testimonios</h3>
                <p>Lo que dicen nuestros representados</p>
            </div>
            <ul class="testimonialsList">
                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>

                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>

                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>

                <!-- Client -->
                <li class="item">
                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra id nunc at finibus.
                        Etiam sollicitudin faucibus cursus. Proin luctus cursus nulla sed iaculis. Quisque vestibulum
                        augue nec aliquet aliquet."</p>
                    <div class="clientname">Jhon Doe</div>
                    <div class="clientinfo">CEO - Company Inc</div>
                </li>
            </ul>
        </div>
    </div>--}}

    <!-- Blog Section -->
    <div id="blog">
        <div class="container">
            <!-- SECTION TITLE -->
            <div class="section-title">
                <h3>Últimas<span> Publicaciones</span></h3>
            </div>
            <ul class="blogGrid">
                @foreach($posts as $post)
                    <li class="item">
                        <div class="int">
                            <!-- Blog Image -->
                            <div class="postimg"><img src="{{ asset('/storage/'.$post->image) }}" alt="Blog Title"></div>
                            <!-- Blog info -->
                            <div class="post-header">
                                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Sep 25, 2017</div>
                                <h4><a href="{{ route('blog.show', $post->id) }}">{{ $post->title }}</a></h4>
                                <div class="postmeta">Por: <span>{{ $post->autor }}</span>
                                </div>
                            </div>
                            <p>{{ $post->excerpt }}</p>
                            <a href="#." class="readmore">Más</a></div>
                    </li>
                @endforeach


            </ul>
        </div>
    </div>

@endsection
