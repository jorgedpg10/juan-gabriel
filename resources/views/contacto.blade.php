@extends('layouts.secondary')

@section('title', 'Contacto')

@section('content')
    <!-- Page Title start -->
    <div class="pageTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1 class="page-heading">Contacto</h1>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="breadCrumb"><a href="{{ route('index') }}">Inicio</a> / <span>Contacto</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title End -->

    {{-- Map start--}}
    <div class="contenedor-mapa">

                    {{--<img src="{{ asset('/imgs/contacto/mapa-1.png') }}" alt="mapa">--}}

    </div>
    {{--Map end--}}

    <!-- Contact Section -->
    <div id="contact" class="parallax-section">
        <div class="container">

            <!-- SECTION TITLE -->
            <div class="section-title">
                <h3>Contáctenos</h3>
                <p>Agende su cita por uno de los siguientes medios:</p>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="contact-now">
                        <div class="contact"> <span><i class="fa fa-home"></i></span>
                            <div class="information"> <strong>Dirección:</strong>
                                <p>24 de Mayo y Emiliano Ortega, Edificio del Río, Of. 7</p>
                            </div>
                        </div>
                        <!-- Contact Info -->
                        <div class="contact"> <span><i class="fa fa-envelope"></i></span>
                            <div class="information"> <strong>Email:</strong>
                                <p>jgperalta12@gmail.com</p>

                            </div>
                        </div>
                        <!-- Contact Info -->
                        <div class="contact"> <span><i class="fa fa-phone"></i></span>
                            <div class="information"> <strong>Teléfonos:</strong>
                                <p>(07)2722218</p>
                            </div>
                        </div>
                        <!-- Contact Info -->
                    </div>
                </div>
                <div class="col-md-8">
                    <!-- CONTACT FORM HERE -->
                    <div class="contact-form">
                        <form id="contact-form" class="row">
                            <div class="col-md-4 col-sm-6">
                                <input type="text" class="form-control" name="name" placeholder="Nombre">
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <input type="tel" class="form-control" name="phone" placeholder="Teléfono">
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <input type="text" class="form-control" name="phone" placeholder="Dirección">
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <textarea class="form-control" rows="5" name="message" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="col-md-12">
                                <button id="submit" type="submit" class="form-control" name="submit">Enviar mensaje</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
