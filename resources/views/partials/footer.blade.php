<!-- Footer Section -->
<div class="site-footer">
    <!-- Footer Top start -->
    <div class="footer-top-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf">
                        <h3 class="footer-logo"><img src="{{ asset('imgs/logo/logo.jpg') }}" class="logo1" alt="LawFirm"></h3>
                        <p>Jus Gentium Law firm, una firma responsable. Soluciones jurídicas en Loja - Ecuador</p>
                        <ul class="footer-contact">
                            <li><i class="fa fa-phone"></i>0984527184</li>
                            <li><i class="fa fa-envelope"></i> jgperalta12@gmail.com</li>
                            <li><i class="fa fa-fax"></i> (07)2722218</li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf footer-menu">
                        <h3 class="footer-lwf-title">Mapa del sitio</h3>
                        <ul>
                            <li><a href="{{ route('nosotros') }}">Nosotros</a></li>
                            <li><a href="{{ route('blog.index') }}">Blog</a></li>
                            <li><a href="{{ route('juan') }}">Ab. Juan Gabriel Peralta</a></li>
                            <li><a href="{{ route('contacto') }}">Contacto</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf footer-menu">
                        <h3 class="footer-lwf-title">Dirección</h3>
                        <p>24 de Mayo y Emiliano Ortega, Edificio del Río, Of. 7</p>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-lwf">
                        <h3 class="footer-lwf-title">Consultas</h3>
                        <ul class="open-hours">
                            <li>Atendemos únicamente previa cita</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer top end -->

    <!-- copyright start -->
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-6">Copyright &copy; 2021 <span>JG Law Firm</span>. Todos los derechos reservados</div>
            </div>
        </div>
    </div>
    <!-- copyright end -->
</div>
