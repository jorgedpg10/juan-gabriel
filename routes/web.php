<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/nosotros', function () { return view('nosotros.nosotros'); })->name('nosotros');

Route::get('/nosotros/juan-peralta', function () { return view('nosotros.juan'); })->name('juan');

Route::get('/blog', [BlogController::class, 'index'])->name('blog.index');
Route::get('/blog/{blog}', [BlogController::class, 'show'])->name('blog.show'); // /blog/1

Route::get('/contacto', function () { return view('contacto'); })->name('contacto');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
